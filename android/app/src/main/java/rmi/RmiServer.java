package rmi;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

public class RmiServer {

    public Map<String, String> intfClassToImplClass = Collections.synchronizedMap(new HashMap<String, String>());
    private Map<String, Object> serviceNameToImpl = Collections.synchronizedMap(new HashMap<String, Object>());
    private int serverPort = 0;
    private ServerSocket serverSocket = null;
    private Thread serverThread = null;
    private boolean isRunning = false;

    public boolean isRunning() {
        return isRunning;
    }

    public void start(int port) {
        if (isRunning()) {
            throw new RmiException("Server already started on port " + serverPort);
        }

        isRunning = true;
        serverPort = port;
        serverThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    serverSocket = new ServerSocket(RmiServer.this.serverPort);
                } catch (Exception e) {
                    throw new RmiException(e);
                }

                while (!Thread.interrupted()) {
                    try {
                        Socket clientSocket = serverSocket.accept();
                        Thread clientThread = new Thread(new RpcHandler(clientSocket));
                        clientThread.start();
                    } catch (Exception e) {
                        throw new RmiException(e);
                    }
                }
                isRunning = false;
            }
        });
        serverThread.start();
    }

    public void stop() {
        serverThread.interrupt();
    }

    public void registerService(String instanceId, Object serviceImpl) {
        serviceNameToImpl.put(instanceId, serviceImpl);
    }

    public void registerClassInterfaceToImplementation(Class<?> myInterface, Class<?> myImplementation) {
        intfClassToImplClass.put(myInterface.getName(), myImplementation.getName());
    }

    public boolean isServiceRegistered(String serviceName) {
        return serviceNameToImpl.containsKey(serviceName);
    }

    public void unregisterService(String serviceName) {
        serviceNameToImpl.remove(serviceName);
    }

    private void tryReadWriteObjects(Socket clientSocket) {
        try {
            RmiRequest remoteRequest = readRequestObject(clientSocket);
            RmiResponse remoteResponse;
            switch (remoteRequest.getType()) {
                case RmiRequest.REGISTER_CLIENT:
                    String clientIp = clientSocket.getRemoteSocketAddress().toString().split("/")[1].split(":")[0];
                    System.out.println("Client Ip address is " + clientIp);
                    remoteResponse = registerClient(clientIp);
                    break;
                default:
                    remoteResponse = handleMethodCall(remoteRequest);
            }
            writeResponseObject(clientSocket, remoteResponse);
        } catch (Exception e) {
            throw new RmiException(e);
        }
    }

    private void writeResponseObject(Socket clientSocket, RmiResponse remoteResponse) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(clientSocket.getOutputStream());
        oos.writeObject(remoteResponse);
        oos.flush();
    }

    private RmiRequest readRequestObject(Socket clientSocket) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(clientSocket.getInputStream());
        RmiRequest remoteRequest = (RmiRequest) ois.readObject();
        return remoteRequest;
    }

    private RmiResponse handleMethodCall(RmiRequest remoteRequest) {

        switch (remoteRequest.getType()) {
            case RmiRequest.METHOD_INVOCATION:
                return invokeMethod(remoteRequest);
            case RmiRequest.CREATE_OBJECT:
                return createObject(remoteRequest);
            case RmiRequest.GET_SINGLETON:
                return createSingletonObject(remoteRequest);
            case RmiRequest.DESTROY_OBJECT:
                return destroyObject(remoteRequest);
            default:
                return null;
        }
    }

    private RmiResponse invokeMethod(RmiRequest remoteRequest) {
        String instanceId = checkServiceName(remoteRequest);

        Object serviceImpl = serviceNameToImpl.get(instanceId);
        Class<?> serviceImplClass = serviceImpl.getClass();

        checkServiceInterface(remoteRequest, serviceImplClass);

        String methodName = remoteRequest.getMethodName();
        Class<?>[] argTypes = remoteRequest.getArgTypes();

        Method method = null;
        try {
            method = serviceImplClass.getMethod(methodName, argTypes);
        } catch (Exception e) {
            throw new RmiException(e);
        }

        Object[] args = remoteRequest.getArgs();
        Object returnValue = null;
        Exception exception = null;
        try {
            if (!method.isAccessible()) {
                method.setAccessible(true);
            }
            System.out.println(remoteRequest.getInterfaceName() + " Invoked " + methodName);
            returnValue = method.invoke(serviceImpl, args);


        } catch (Exception e) {
            exception = e;
            e.printStackTrace();
        }

        RmiResponse rmiResponse = new RmiResponse();

        if (exception != null) {
            rmiResponse.setException(exception);
        } else {
            rmiResponse.setReturnValue(returnValue);
        }
        return rmiResponse;
    }

    private RmiResponse createObject(RmiRequest remoteRequest) {
        String instanceId = System.nanoTime() + "";
        Class<?>[] argTypes = remoteRequest.getArgTypes();
        Object[] args = remoteRequest.getArgs();

        Object newObject = null;
        Exception exception = null;

        try {
            String className = intfClassToImplClass.get(remoteRequest.getInterfaceName());
            Class<?> classType = Class.forName(className);
            if (argTypes.length != 0) {
                Constructor ctor = classType.getConstructor(argTypes);
                ctor.setAccessible(true);
                newObject = ctor.newInstance(args);
            } else {
                newObject = classType.newInstance();
                System.out.println(newObject);
            }
            System.out.println("Created " + newObject);

        } catch (Exception e) {
            e.printStackTrace();
            exception = e;
        }
        registerService(instanceId, newObject);
        RmiResponse rmiResponse = new RmiResponse();

        if (exception != null) {
            rmiResponse.setException(exception);
        } else {
            rmiResponse.setReturnValue(instanceId);
        }
        return rmiResponse;
    }

    private RmiResponse createSingletonObject(RmiRequest remoteRequest) {

        Exception exception = null;
        String instanceId = null;

        try {
            String className = intfClassToImplClass.get(remoteRequest.getInterfaceName());
            instanceId = className;

            if (!serviceNameToImpl.containsKey(instanceId)) {
                Class<?> classType = Class.forName(className);
                Method getInstance = classType.getMethod("getInstance");
                Object newObject = getInstance.invoke(null);
                registerService(instanceId, newObject);
                System.out.println("Singleton Created " + newObject);

            }

        } catch (Exception e) {
            e.printStackTrace();
            exception = e;
        }

        RmiResponse rmiResponse = new RmiResponse();

        if (exception != null) {
            rmiResponse.setException(exception);
        } else {
            rmiResponse.setReturnValue(instanceId);
        }
        return rmiResponse;
    }

    private RmiResponse destroyObject(RmiRequest remoteRequest) {
        return null;
    }

    private RmiResponse registerClient(String ip) {
        RmiResponse rmiResponse = new RmiResponse();
        rmiResponse.setReturnValue(RmiClient.setHostIp(ip));
        return rmiResponse;
    }

    private void checkServiceInterface(RmiRequest remoteRequest, Class<?> serviceImplClass) {
        String interfaceCName = remoteRequest.getInterfaceName();
        Class<?> interfaceClass = null;
        try {
            interfaceClass = Class.forName(interfaceCName);
        } catch (ClassNotFoundException e) {
            throw new RmiException(e);
        }

        Class<?>[] interfaces = serviceImplClass.getInterfaces();
        int idx = Arrays.binarySearch(interfaces, interfaceClass, new Comparator<Class<?>>() {
            @Override
            public int compare(Class<?> o1, Class<?> o2) {
                return o1.equals(o2) ? 0 : -1;
            }
        });
        if (idx < 0) {
            throw new RmiException(interfaceCName + " not implemeted by service with name "
                    + remoteRequest.getInstanceId());
        }
    }

    private String checkServiceName(RmiRequest remoteRequest) {
        String serviceName = remoteRequest.getInstanceId();
        if (!serviceNameToImpl.containsKey(serviceName)) {
            throw new RmiException(serviceName + " is not an instance of " + remoteRequest.getInterfaceName());
        }
        return serviceName;
    }

    private class RpcHandler implements Runnable {

        private Socket clientSocket = null;

        public RpcHandler(Socket clientSocket) {
            this.clientSocket = clientSocket;
        }

        @Override
        public void run() {
            tryReadWriteObjects(clientSocket);
        }
    }
}
