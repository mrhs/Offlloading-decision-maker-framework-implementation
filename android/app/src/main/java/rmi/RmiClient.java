package rmi;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.Socket;

/**
 * RcpClient
 *
 * @author h.indzhov
 */
public class RmiClient implements InvocationHandler {

    protected static final int PORT = 6789;
    protected static String hostIp = null;
    private static Boolean isRegisteredOnServer = false;

    public static void manualServerRegistration(String serverIp) {
        RmiClient.isRegisteredOnServer = true;
        hostIp = serverIp;
    }

    private String instanceId = "";
    private Class<?> interfaceClass = null;

    private RmiClient() {
        if (!isRegisteredOnServer) {
            if (registerOnServer())
                isRegisteredOnServer = true;
            else
                throw new RmiException("Can't register on server");
        }
    }

    public static Boolean setHostIp(String ip) {
        if (hostIp == null) {
            hostIp = ip;
            return true;
        } else {
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T constructor(Class<T> interfaceClass, Class<?>[] argTypes, Object[] args) {
        RmiClient remoteClient = new RmiClient();
        remoteClient.interfaceClass = interfaceClass;
        remoteClient.instanceId = remoteClient.initializeObject(interfaceClass.getName(), argTypes, args);
        return (T) Proxy.newProxyInstance(interfaceClass.getClassLoader(),
                new Class<?>[]{interfaceClass}, remoteClient);
    }

    @SuppressWarnings("unchecked")
    public static <T> T constructor(Class<T> interfaceClass, Boolean singleton) {
        RmiClient remoteClient = new RmiClient();
        remoteClient.interfaceClass = interfaceClass;
        if (singleton) {
            remoteClient.instanceId = remoteClient.getSingletonObject(interfaceClass.getName());
        } else {
            remoteClient.instanceId = remoteClient.initializeObject(interfaceClass.getName(),
                    new Class<?>[]{}, new Object[]{});
        }

        return (T) Proxy.newProxyInstance(interfaceClass.getClassLoader(),
                new Class<?>[]{interfaceClass}, remoteClient);
    }

    private static Boolean registerOnServer() {
        try {
            RmiRequest rmiRequest = new RmiRequest();

            Socket clientSocket = new Socket(hostIp, PORT);
            writeRequestObject(rmiRequest, clientSocket);
            RmiResponse rmiResponse = readResponseObject(clientSocket);

            if (!rmiResponse.isSuccessfull()) {
                throw new RmiException(rmiResponse.getException());
            }
            return (Boolean) rmiResponse.getReturnValue();

        } catch (Exception e) {
            throw new RmiException(e);
        }
    }

    private static RmiResponse readResponseObject(Socket clientSocket)
            throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(
                clientSocket.getInputStream());
        RmiResponse rmiResponse = (RmiResponse) ois.readObject();
        clientSocket.shutdownInput();
        clientSocket.close();
        return rmiResponse;
    }

    private static void writeRequestObject(RmiRequest rmiRequest, Socket clientSocket)
            throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(
                clientSocket.getOutputStream());
        oos.writeObject(rmiRequest);
        clientSocket.shutdownOutput();
    }

    private String initializeObject(String interfaceClassName, Class<?>[] argTypes, Object[] args) {
        try {
            RmiRequest rmiRequest = new RmiRequest(interfaceClassName, argTypes, args);

            Socket clientSocket = new Socket(hostIp, PORT);
            writeRequestObject(rmiRequest, clientSocket);
            RmiResponse rmiResponse = readResponseObject(clientSocket);

            if (!rmiResponse.isSuccessfull()) {
                throw new RmiException(rmiResponse.getException());
            }
            return (String) rmiResponse.getReturnValue();

        } catch (Exception e) {
            throw new RmiException(e);
        }

    }

    private String getSingletonObject(String interfaceClassName) {
        try {
            RmiRequest rmiRequest = new RmiRequest(interfaceClassName);

            Socket clientSocket = new Socket(hostIp, PORT);
            writeRequestObject(rmiRequest, clientSocket);
            RmiResponse rmiResponse = readResponseObject(clientSocket);

            if (!rmiResponse.isSuccessfull()) {
                throw new RmiException(rmiResponse.getException());
            }
            return (String) rmiResponse.getReturnValue();

        } catch (Exception e) {
            throw new RmiException(e);
        }

    }

    public Object invoke(Object proxy, Method method, Object[] args) {
        try {
            String interfaceName = interfaceClass.getName();

            String methodName = method.getName();
            Class<?>[] argTypes = method.getParameterTypes();

            RmiRequest rmiRequest = new RmiRequest(instanceId, interfaceName, methodName, argTypes, args);

            Socket clientSocket = new Socket(hostIp, PORT);
            writeRequestObject(rmiRequest, clientSocket);
            RmiResponse rmiResponse = readResponseObject(clientSocket);

            if (!rmiResponse.isSuccessfull()) {
                throw new RmiException(rmiResponse.getException());
            }

            Object returnValue = rmiResponse.getReturnValue();
            return returnValue;
        } catch (Exception e) {
            throw new RmiException(e);
        }
    }

}
