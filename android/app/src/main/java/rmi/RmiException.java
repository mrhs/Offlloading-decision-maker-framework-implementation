package rmi;

/**
 * An exception wrapper class. In case you need to be able to tell where 
 * it came from later or just hide the original exception.  
 * @author h.indzhov
 */
public class RmiException extends RuntimeException {

	private static final long serialVersionUID = -7053882704721668233L;
	
	public RmiException(Exception e) {
		super(e);
	}

	public RmiException(String string) {
		super(string);
	}

}
