package decision;

import android.util.Log;

import classes.Utils.UtilsProxy;
import classes.signal.SignalProxy;
import classes.signalhandler.SignalHandlerProxy;

import java.util.Timer;
import java.util.TimerTask;

public class DecisionSystem {
    public DecisionSystem() {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                // task to run goes here
                Log.d("Profiler", SignalProxy.class.getSimpleName() + " local execution time " + SignalProxy.getTotalLocalTime());
                Log.d("Profiler", SignalHandlerProxy.class.getSimpleName() + " local execution time " + SignalHandlerProxy.getTotalLocalTime());
                Log.d("Profiler", UtilsProxy.class.getSimpleName() + " local execution time " + UtilsProxy.getTotalLocalTime());
                Log.d("Profiler", SignalProxy.class.getSimpleName() + " suspension time " + SignalProxy.getTotalSuspenseTime());
                Log.d("Profiler", SignalHandlerProxy.class.getSimpleName() + " suspension time " + SignalHandlerProxy.getTotalSuspenseTime());
                Log.d("Profiler", UtilsProxy.class.getSimpleName() + " suspension time " + UtilsProxy.getTotalSuspenseTime());

                SignalProxy.resetTotalLocalTime();
                SignalProxy.resetTotalSuspendedTime();

                UtilsProxy.resetTotalLocalTime();
                UtilsProxy.resetTotalSuspendedTime();

                SignalHandlerProxy.resetTotalLocalTime();
                SignalHandlerProxy.resetTotalSuspendedTime();
            }
        };
        Timer timer = new Timer();
        long delay = 0;
        long intevalPeriod = 10 * 1000;
        // schedules the task to be run in an interval
        timer.scheduleAtFixedRate(task, delay, intevalPeriod);
    }
}
