package com.my.mrhs.opencv;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;

import classes.Utils.UtilsIntf;
import classes.Utils.UtilsProxy;
import classes.signal.SignalIntf;
import classes.signal.SignalProxy;
import classes.signalhandler.SignalHandlerIntf;
import classes.signalhandler.SignalHandlerProxy;
import decision.DecisionSystem;
import rmi.RmiClient;
import rmi.RmiServer;


public class MainActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    //********************************** STATIC **********************************
    private static final int MAT_WIDTH = 340;
    private static final int MAT_HEIGHT = 150;
    // Used for logging success or failure messages
    private static final String TAG = "OCVSample::Activity";
    //********************************** DATA **********************************
    LinearLayout linearLayout;
    DecisionSystem decisionSystem;

    Mat tmpMat;

    // Loads camera view of OpenCV for us to use. This lets us see using OpenCV
    private Tutorial3View mOpenCvCameraView;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    public MainActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        setupServer();

        SignalProxy. setExecutionLocal();
        SignalHandlerProxy.setExecutionLocal();
        UtilsProxy.setExecutionOffloaded();

//        imgView = (ImageView) findViewById(R.id.imgView);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.relativeLayout);
        mOpenCvCameraView = new Tutorial3View(this, 0);
        mOpenCvCameraView.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.FILL_PARENT, ConstraintLayout.LayoutParams.FILL_PARENT));
        layout.addView(mOpenCvCameraView);

        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.enableFpsMeter();
        mOpenCvCameraView.setCvCameraViewListener(this);

        decisionSystem = new DecisionSystem();
    }

    private void setupServer(){
        RmiClient.manualServerRegistration("192.168.199.41");

        RmiServer rmiServer = new RmiServer();
        rmiServer.registerClassInterfaceToImplementation(UtilsIntf.class, UtilsProxy.class);
        rmiServer.registerClassInterfaceToImplementation(SignalHandlerIntf.class, SignalHandlerProxy.class);
        rmiServer.registerClassInterfaceToImplementation(SignalIntf.class, SignalProxy.class);
        rmiServer.start(6789);
        Log.d("Server", "Server ready");
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        tmpMat = new Mat(288, 352, 24);
        mOpenCvCameraView.setResolution(mOpenCvCameraView.getResolutionList().get(4));
        UtilsProxy.getInstance().initialize();
    }

    public void onCameraViewStopped() {
        UtilsProxy.getInstance().destroy();
    }

    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
//        return convertByteArray2Mat(convertMat2ByteArray(inputFrame.rgba()));
        return convertByteArray2Mat(UtilsProxy.getInstance().processMat(convertMat2ByteArray(inputFrame.rgba())));
    }

    private Mat convertByteArray2Mat(byte[] array) {
        tmpMat.put(0, 0, array);
        return tmpMat;
    }

    private byte[] convertMat2ByteArray(Mat mat) {
        byte[] return_buff = new byte[(int) (mat.total() * mat.elemSize())];
        mat.get(0, 0, return_buff);
        return return_buff;
    }
}