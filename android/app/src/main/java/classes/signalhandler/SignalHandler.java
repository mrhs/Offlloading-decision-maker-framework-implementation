package classes.signalhandler;



import classes.signal.SignalProxy;
import org.opencv.core.Point;

import java.util.ArrayList;

public class SignalHandler implements SignalHandlerIntf {

    //********************************** STATIC **********************************
    final static int LOCATION_TOLERANCE = 30;
    final static int AREA_TOLERANCE = 50;
    final static int SIGNAL_TIMEOUT_MILI = 5000;
    final static int SIGNAL_SIGNAL_LIST_TIMEOUT_MILI = 10000;
    final static int SIGNAL_HANDLER_TIMEOUT_MILI = 1000;

    //********************************** DATA **********************************
    private Long lastUpdateTime;
    private ArrayList<Long> seenTimeList;
    private ArrayList<SignalProxy> signalsList;

    //********************************** FUNCTION **********************************
    public SignalHandler(){
        this.seenTimeList = new ArrayList<>();
        this.signalsList = new ArrayList<>();
        this.lastUpdateTime = (long) 0;
    }


    @Override
    public void setStep(ArrayList<Point> centers, ArrayList<Double> areas) {
        Long time = System.currentTimeMillis();
        seenTimeList.add(time);
        checkForUpdate(time);
        ArrayList<SignalProxy> tmpsignalsList = new ArrayList<>();
        for (int i = 0; i < centers.size(); i++) {
//            int index = findMostSimilarSignalIndex(signalsList, centers.get(i), areas.get(i), LOCATION_TOLERANCE);
            ArrayList<Integer> indexes = findNearSignalsIndexes(signalsList, centers.get(i), LOCATION_TOLERANCE);
            int index = -1;
            if (indexes.size() > 0) {
                index = indexes.get(0);
            }
            if (index == -1) {
                tmpsignalsList.add(new SignalProxy(centers.get(i), areas.get(i), time));
            } else {
                signalsList.get(index).setCurrentCondition(centers.get(i), areas.get(i));
                signalsList.get(index).addToSeenTimeList(time);
            }
        }
        signalsList.addAll(tmpsignalsList);
    }

    
    @Override
    public ArrayList<SignalProxy> getDesignatedSignal(float f, float f_tolerance, float dc, float dc_tolerance) {
        ArrayList<SignalProxy> tmpSignalsList = new ArrayList<>();
        for (SignalProxy signal : signalsList) {
            if (signal.getFrequency() >= f - f_tolerance && signal.getFrequency() <= f + f_tolerance) {
                if (signal.getDutyCycle() >= dc - dc_tolerance && signal.getDutyCycle() <= dc + dc_tolerance) {
                    tmpSignalsList.add(signal);
                }
            }
        }
        return tmpSignalsList;
    }

    @Override
    public void checkForUpdate(Long time) {
        if (time - lastUpdateTime > SIGNAL_HANDLER_TIMEOUT_MILI) {
            updateSignal(time);
            lastUpdateTime = time;
        }
    }


    @Override
    public void updateSignal(Long time) {
        ArrayList<SignalProxy> tmpSignalsList = new ArrayList<>();
        for (SignalProxy signal : signalsList) {
            if (time - signal.getLastSeenTime() < SIGNAL_TIMEOUT_MILI) {
                signal.reduceTimeList(seenTimeList, calculateHalfAveragePeriodLength());
                signal.clearOldSignals(time - SIGNAL_SIGNAL_LIST_TIMEOUT_MILI);
                tmpSignalsList.add(signal);
            }
        }
        signalsList = tmpSignalsList;
    }

    //********************** Setter **********************
    
    @Override
    public void setLastUpdateTime(Long lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    @Override
    public void setSeenTimeList(ArrayList<Long> seenTimeList) {
        this.seenTimeList = seenTimeList;
    }

    @Override
    public void setSignalsList(ArrayList<SignalProxy> signalsList) {
        this.signalsList = signalsList;
    }

    //********************** Getter **********************
    @Override
    public Long getLastUpdateTime() {
        return lastUpdateTime;
    }

    @Override
    public ArrayList<Long> getSeenTimeList() {
        return seenTimeList;
    }

    @Override
    public ArrayList<SignalProxy> getSignalsList() {
        return signalsList;
    }

    //***************************** Utility functions *****************************
    @Override
    public Long calculateHalfAveragePeriodLength() {
        final int PERIOD_COUNT = 20;
        int index = 0;
        int sum = 0;
        int count = 0;
        if (seenTimeList.size() > PERIOD_COUNT) {
            index = seenTimeList.size() - PERIOD_COUNT;
        }
        while (index + 1 < seenTimeList.size()) {
            sum += seenTimeList.get(index + 1) - seenTimeList.get(index);
            count++;
            index++;
        }
        if (count > 0)
            return new Long(sum / (2 * count));
        else
            return new Long(0);
    }

    @Override
    public int findMostSimilarSignalIndex(ArrayList<SignalProxy> points, Point center, Double area, Integer tolerance) {
        //TODO here integrate area in finding the closes signal
        for (int i : findNearSignalsIndexes(points, center, tolerance)) {
            if (Math.abs(points.get(i).getCurrentArea() - area) < AREA_TOLERANCE) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public ArrayList<Integer> findNearSignalsIndexes(ArrayList<SignalProxy> points, Point center, Integer tolerance) {
        ArrayList<Integer> indexList = new ArrayList<>();
        ArrayList<Float> distanceList = new ArrayList<>();
        Float distance;
        for (int i = 0; i < points.size(); i++) {
            distance = calculateDistance(points.get(i).getCurrentLocation(), center);
            if (distance <= tolerance) {
                if (indexList.size() == 0) {
                    indexList.add(i);
                    distanceList.add(distance);
                } else {
                    for (int j = 0; j < distanceList.size(); j++) {
                        if (distance > distanceList.get(j)) {
                            indexList.add(j, i);
                            distanceList.add(j, distance);
                            break;
                        }
                        if (j == distanceList.size() - 1) {
                            indexList.add(i);
                            distanceList.add(distance);
                            break;
                        }
                    }
                }
            }
        }
        return indexList;
    }

    @Override
    public float calculateDistance(Point a, Point b) {
        double xDiff = a.x - b.x;
        double yDiff = a.y - b.y;
        return (float) Math.sqrt(Math.pow(xDiff, 2) + Math.pow(yDiff, 2));
    }
}
