package classes.signalhandler;

import classes.signal.SignalProxy;
import org.opencv.core.Point;

import java.util.ArrayList;

/**
 * Created by Muhammad Reza on 21/08/2018.
 */

public interface SignalHandlerIntf {
    void setStep(ArrayList<Point> centers, ArrayList<Double> areas);

    ArrayList<SignalProxy> getDesignatedSignal(float f, float f_tolerance, float dc, float dc_tolerance);

    void checkForUpdate(Long time);

    void updateSignal(Long time);

    void setLastUpdateTime(Long lastUpdateTime);

    void setSeenTimeList(ArrayList<Long> seenTimeList);

    void setSignalsList(ArrayList<SignalProxy> signalsList);

    //********************** Getter **********************
    Long getLastUpdateTime();

    ArrayList<Long> getSeenTimeList();

    ArrayList<SignalProxy> getSignalsList();

    //***************************** Utility functions *****************************
    Long calculateHalfAveragePeriodLength();

    int findMostSimilarSignalIndex(ArrayList<SignalProxy> points, Point center, Double area, Integer tolerance);

    ArrayList<Integer> findNearSignalsIndexes(ArrayList<SignalProxy> points, Point center, Integer tolerance);

    float calculateDistance(Point a, Point b);
}
