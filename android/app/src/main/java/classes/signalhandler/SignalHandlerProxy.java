package classes.signalhandler;

import org.opencv.core.Point;

import java.util.ArrayList;

import classes.Proxy;
import classes.signal.SignalProxy;
import rmi.RmiClient;

/**
 * Created by Muhammad Reza on 21/08/2018.
 */

public class SignalHandlerProxy extends Proxy<SignalHandlerIntf> implements SignalHandlerIntf {

    //****************************** Profiling *************************************
    protected static Integer totalLocalTime = 0;
    protected static Integer totalSuspendedTime = 0;

    protected static Integer idCounter = 0;

    protected static Integer executionCondition = BOTH; //At the beginning the execution will be on both server and local

    //********************** Setter **********************

    public synchronized static void setExecutionLocal() {
        executionCondition = LOCAL;
    }

    public synchronized static void setExecutionOffloaded() {
        executionCondition = OFFLOADED;
    }

    public synchronized static void setExecutionBoth() {
        executionCondition = BOTH;
    }

    @Override
    public synchronized void incrementTotalLocalTime(Integer totalLocalTimeV) {
        totalLocalTime += totalLocalTimeV;
    }

    @Override
    public synchronized void incrementTotalSuspendedTime(Integer totalSuspendedTimeV) {
        totalSuspendedTime += totalSuspendedTimeV;
    }

    public synchronized static void resetTotalLocalTime() {
        totalLocalTime = 0;
    }

    public synchronized static void resetTotalSuspendedTime() {
        totalSuspendedTime = 0;
    }


    //********************** Getter **********************

    public static Integer getTotalLocalTime() {
        return totalLocalTime;
    }

    public static Integer getTotalSuspenseTime() {
        return totalSuspendedTime;
    }

    public static Integer getTotalOffloadedTime() {
        return 0;
    }

    ;

    //******************************************************************************

    public SignalHandlerProxy() {
        super(SignalHandlerIntf.class.getSimpleName() + idCounter);
        idCounter++;
        local = new SignalHandler();
        server = RmiClient.constructor(SignalHandlerIntf.class, false);
    }

    @Override
    public void setStep(ArrayList<Point> centers, ArrayList<Double> areas) {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.setStep(centers, areas);
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.setStep(centers, areas);
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.setStep(centers, areas);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.setStep(centers, areas);
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public ArrayList<SignalProxy> getDesignatedSignal(float f, float f_tolerance, float dc, float dc_tolerance) {
        ArrayList<SignalProxy> list;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                list = local.getDesignatedSignal(f, f_tolerance, dc, dc_tolerance);
                stopLocalTimeCount();
                return list;
            case OFFLOADED:
                startSuspenseTimeCount();
                list = server.getDesignatedSignal(f, f_tolerance, dc, dc_tolerance);
                stopSuspenseTimeCount();
                return list;
            default:
                startLocalTimeCount();
                list = local.getDesignatedSignal(f, f_tolerance, dc, dc_tolerance);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.getDesignatedSignal(f, f_tolerance, dc, dc_tolerance);
                stopSuspenseTimeCount();
                return list;
        }
    }

    @Override
    public void checkForUpdate(Long time) {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.checkForUpdate(time);
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.checkForUpdate(time);
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.checkForUpdate(time);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.checkForUpdate(time);
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public void updateSignal(Long time) {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.updateSignal(time);
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.updateSignal(time);
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.updateSignal(time);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.updateSignal(time);
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public void setLastUpdateTime(Long lastUpdateTime) {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.setLastUpdateTime(lastUpdateTime);
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.setLastUpdateTime(lastUpdateTime);
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.setLastUpdateTime(lastUpdateTime);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.setLastUpdateTime(lastUpdateTime);
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public void setSeenTimeList(ArrayList<Long> seenTimeList) {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.setSeenTimeList(seenTimeList);
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.setSeenTimeList(seenTimeList);
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.setSeenTimeList(seenTimeList);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.setSeenTimeList(seenTimeList);
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public void setSignalsList(ArrayList<SignalProxy> signalsList) {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.setSignalsList(signalsList);
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.setSignalsList(signalsList);
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.setSignalsList(signalsList);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.setSignalsList(signalsList);
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public Long getLastUpdateTime() {
        Long time;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                time = local.getLastUpdateTime();
                stopLocalTimeCount();
                return time;
            case OFFLOADED:
                startSuspenseTimeCount();
                time = server.getLastUpdateTime();
                stopSuspenseTimeCount();
                return time;
            default:
                startLocalTimeCount();
                time = local.getLastUpdateTime();
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.getLastUpdateTime();
                stopSuspenseTimeCount();
                return time;
        }
    }

    @Override
    public ArrayList<Long> getSeenTimeList() {
        ArrayList<Long> list;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                list = local.getSeenTimeList();
                stopLocalTimeCount();
                return list;
            case OFFLOADED:
                startSuspenseTimeCount();
                list = server.getSeenTimeList();
                stopSuspenseTimeCount();
                return list;
            default:
                startLocalTimeCount();
                list = local.getSeenTimeList();
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.getSeenTimeList();
                stopSuspenseTimeCount();
                return list;
        }
    }

    @Override
    public ArrayList<SignalProxy> getSignalsList() {
        ArrayList<SignalProxy> list;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                list = local.getSignalsList();
                stopLocalTimeCount();
                return list;
            case OFFLOADED:
                startSuspenseTimeCount();
                list = server.getSignalsList();
                stopSuspenseTimeCount();
                return list;
            default:
                startLocalTimeCount();
                list = local.getSignalsList();
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.getSignalsList();
                stopSuspenseTimeCount();
                return list;
        }
    }

    @Override
    public Long calculateHalfAveragePeriodLength() {
        Long num;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                num = local.calculateHalfAveragePeriodLength();
                stopLocalTimeCount();
                return num;
            case OFFLOADED:
                startSuspenseTimeCount();
                num = server.calculateHalfAveragePeriodLength();
                stopSuspenseTimeCount();
                return num;
            default:
                startLocalTimeCount();
                num = local.calculateHalfAveragePeriodLength();
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.calculateHalfAveragePeriodLength();
                stopSuspenseTimeCount();
                return num;
        }
    }

    @Override
    public int findMostSimilarSignalIndex(ArrayList<SignalProxy> points, Point center, Double area, Integer tolerance) {
        int num;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                num = local.findMostSimilarSignalIndex(points, center, area, tolerance);
                stopLocalTimeCount();
                return num;
            case OFFLOADED:
                startSuspenseTimeCount();
                num = server.findMostSimilarSignalIndex(points, center, area, tolerance);
                stopSuspenseTimeCount();
                return num;
            default:
                startLocalTimeCount();
                num = local.findMostSimilarSignalIndex(points, center, area, tolerance);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.findMostSimilarSignalIndex(points, center, area, tolerance);
                stopSuspenseTimeCount();
                return num;
        }
    }

    @Override
    public ArrayList<Integer> findNearSignalsIndexes(ArrayList<SignalProxy> points, Point center, Integer tolerance) {
        ArrayList<Integer> list;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                list = local.findNearSignalsIndexes(points, center, tolerance);
                stopLocalTimeCount();
                return list;
            case OFFLOADED:
                startSuspenseTimeCount();
                list = server.findNearSignalsIndexes(points, center, tolerance);
                stopSuspenseTimeCount();
                return list;
            default:
                startLocalTimeCount();
                list = local.findNearSignalsIndexes(points, center, tolerance);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.findNearSignalsIndexes(points, center, tolerance);
                stopSuspenseTimeCount();
                return list;
        }
    }

    @Override
    public float calculateDistance(Point a, Point b) {
        float distance;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                distance = local.calculateDistance(a, b);
                stopLocalTimeCount();
                return distance;
            case OFFLOADED:
                startSuspenseTimeCount();
                distance = server.calculateDistance(a, b);
                stopSuspenseTimeCount();
                return distance;
            default:
                startLocalTimeCount();
                distance = local.calculateDistance(a, b);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.calculateDistance(a, b);
                stopSuspenseTimeCount();
                return distance;
        }
    }
}
