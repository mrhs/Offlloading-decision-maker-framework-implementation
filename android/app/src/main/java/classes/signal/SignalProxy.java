package classes.signal;

import org.opencv.core.Point;

import java.util.ArrayList;

import classes.Proxy;
import rmi.RmiClient;

/**
 * Created by Muhammad Reza on 21/08/2018.
 */

public class SignalProxy extends Proxy<SignalIntf> implements SignalIntf {

    //****************************** Profiling *************************************
    protected static Integer totalLocalTime = 0;
    protected static Integer totalSuspendedTime = 0;

    protected static Integer idCounter = 0;

    protected static Integer executionCondition = BOTH; //At the beginning the execution will be on both server and local

    //********************** Setter **********************

    public synchronized static void setExecutionLocal() {
        executionCondition = LOCAL;
    }

    public synchronized static void setExecutionOffloaded() {
        executionCondition = OFFLOADED;
    }

    public synchronized static void setExecutionBoth() {
        executionCondition = BOTH;
    }

    public synchronized static void resetTotalLocalTime() {
        totalLocalTime = 0;
    }

    public synchronized static void resetTotalSuspendedTime() {
        totalSuspendedTime = 0;
    }

    @Override
    public synchronized void incrementTotalLocalTime(Integer totalLocalTimeV) {
        totalLocalTime += totalLocalTimeV;
    }

    @Override
    public synchronized void incrementTotalSuspendedTime(Integer totalSuspendedTimeV) {
        totalSuspendedTime += totalSuspendedTimeV;
    }

    //********************** Getter **********************

    public static Integer getTotalLocalTime() {
        return totalLocalTime;
    }

    public static Integer getTotalSuspenseTime() {
        return totalSuspendedTime;
    }

    public static Integer getTotalOffloadedTime() {
        return 0;
    }

    ;

    //******************************************************************************

    public SignalProxy(Point currentLocation, Double currentArea, Long firstSeenTime) {
        super(SignalIntf.class.getSimpleName()+idCounter);
        idCounter++;
        local = new Signal(currentLocation, currentArea, firstSeenTime);
        server = RmiClient.constructor(SignalIntf.class,
                new Class<?>[]{Point.class, Double.class, Long.class}, new Object[]{currentLocation, currentArea, firstSeenTime});
    }

    @Override
    public void setCurrentCondition(Point center, Double area) {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.setCurrentCondition(center, area);
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.setCurrentCondition(center, area);
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.setCurrentCondition(center, area);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.setCurrentCondition(center, area);
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public void setCurrentLocation(Point currentLocation) {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.setCurrentLocation(currentLocation);
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.setCurrentLocation(currentLocation);
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.setCurrentLocation(currentLocation);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.setCurrentLocation(currentLocation);
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public void setCurrentArea(double currentArea) {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.setCurrentArea(currentArea);
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.setCurrentArea(currentArea);
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.setCurrentArea(currentArea);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.setCurrentArea(currentArea);
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public void setFirstSeenTime(Long firstSeenTime) {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.setFirstSeenTime(firstSeenTime);
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.setFirstSeenTime(firstSeenTime);
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.setFirstSeenTime(firstSeenTime);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.setFirstSeenTime(firstSeenTime);
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public void setLastSeenTime(Long lastSeenTime) {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.setLastSeenTime(lastSeenTime);
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.setLastSeenTime(lastSeenTime);
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.setLastSeenTime(lastSeenTime);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.setLastSeenTime(lastSeenTime);
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public void addToSeenTimeList(Long timeSeen) {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.addToSeenTimeList(timeSeen);
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.addToSeenTimeList(timeSeen);
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.addToSeenTimeList(timeSeen);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.addToSeenTimeList(timeSeen);
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public void setFrequency(float frequency) {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.setFrequency(frequency);
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.setFrequency(frequency);
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.setFrequency(frequency);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.setFrequency(frequency);
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public void setDutyCycle(float dutyCycle) {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.setDutyCycle(dutyCycle);
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.setDutyCycle(dutyCycle);
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.setDutyCycle(dutyCycle);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.setDutyCycle(dutyCycle);
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public void setSeenTimeList(ArrayList<Long> seenTimeList) {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.setSeenTimeList(seenTimeList);
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.setSeenTimeList(seenTimeList);
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.setSeenTimeList(seenTimeList);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.setSeenTimeList(seenTimeList);
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public void setSignalList(ArrayList<Signal.TimeSlot> signalList) {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.setSignalList(signalList);
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.setSignalList(signalList);
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.setSignalList(signalList);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.setSignalList(signalList);
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public Point getCurrentLocation() {
        Point point;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                point = local.getCurrentLocation();
                stopLocalTimeCount();
                return point;
            case OFFLOADED:
                startSuspenseTimeCount();
                point = server.getCurrentLocation();
                stopSuspenseTimeCount();
                return point;
            default:
                startLocalTimeCount();
                point = local.getCurrentLocation();
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.getCurrentLocation();
                stopSuspenseTimeCount();
                return point;
        }
    }

    @Override
    public double getCurrentArea() {
        double area;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                area = local.getCurrentArea();
                stopLocalTimeCount();
                return area;
            case OFFLOADED:
                startSuspenseTimeCount();
                area = server.getCurrentArea();
                stopSuspenseTimeCount();
                return area;
            default:
                startLocalTimeCount();
                area = local.getCurrentArea();
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.getCurrentArea();
                stopSuspenseTimeCount();
                return area;
        }
    }

    @Override
    public Long getFirstSeenTime() {
        Long seenTime;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                seenTime = local.getFirstSeenTime();
                stopLocalTimeCount();
                return seenTime;
            case OFFLOADED:
                startSuspenseTimeCount();
                seenTime = server.getFirstSeenTime();
                stopSuspenseTimeCount();
                return seenTime;
            default:
                startLocalTimeCount();
                seenTime = local.getFirstSeenTime();
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.getFirstSeenTime();
                stopSuspenseTimeCount();
                return seenTime;
        }
    }

    @Override
    public Long getLastSeenTime() {
        Long seenTime;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                seenTime = local.getLastSeenTime();
                stopLocalTimeCount();
                return seenTime;
            case OFFLOADED:
                startSuspenseTimeCount();
                seenTime = server.getLastSeenTime();
                stopSuspenseTimeCount();
                return seenTime;
            default:
                startLocalTimeCount();
                seenTime = local.getLastSeenTime();
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.getLastSeenTime();
                stopSuspenseTimeCount();
                return seenTime;
        }
    }

    @Override
    public ArrayList<Long> getSeenTimeList() {
        ArrayList<Long> list;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                list = local.getSeenTimeList();
                stopLocalTimeCount();
                return list;
            case OFFLOADED:
                startSuspenseTimeCount();
                list = server.getSeenTimeList();
                stopSuspenseTimeCount();
                return list;
            default:
                startLocalTimeCount();
                list = local.getSeenTimeList();
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.getSeenTimeList();
                stopSuspenseTimeCount();
                return list;
        }
    }

    @Override
    public float getFrequency() {
        float frequency;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                frequency = local.getFrequency();
                stopLocalTimeCount();
                return frequency;
            case OFFLOADED:
                startSuspenseTimeCount();
                frequency = server.getFrequency();
                stopSuspenseTimeCount();
                return frequency;
            default:
                startLocalTimeCount();
                frequency = local.getFrequency();
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.getFrequency();
                stopSuspenseTimeCount();
                return frequency;
        }
    }

    @Override
    public float getDutyCycle() {
        float cycle;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                cycle = local.getDutyCycle();
                stopLocalTimeCount();
                return cycle;
            case OFFLOADED:
                startSuspenseTimeCount();
                cycle = server.getDutyCycle();
                stopSuspenseTimeCount();
                return cycle;
            default:
                startLocalTimeCount();
                cycle = local.getDutyCycle();
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.getDutyCycle();
                stopSuspenseTimeCount();
                return cycle;
        }
    }

    @Override
    public ArrayList<Signal.TimeSlot> getSignalList() {
        ArrayList<Signal.TimeSlot> list;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                list = local.getSignalList();
                stopLocalTimeCount();
                return list;
            case OFFLOADED:
                startSuspenseTimeCount();
                list = server.getSignalList();
                stopSuspenseTimeCount();
                return list;
            default:
                startLocalTimeCount();
                list = local.getSignalList();
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.getSignalList();
                stopSuspenseTimeCount();
                return list;
        }
    }

    @Override
    public float calculateDutyCycle() {
        float cycle;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                cycle = local.calculateDutyCycle();
                stopLocalTimeCount();
                return cycle;
            case OFFLOADED:
                startSuspenseTimeCount();
                cycle = server.calculateDutyCycle();
                stopSuspenseTimeCount();
                return cycle;
            default:
                startLocalTimeCount();
                cycle = local.calculateDutyCycle();
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.calculateDutyCycle();
                stopSuspenseTimeCount();
                return cycle;
        }
    }

    @Override
    public float calculateFrequency() {
        float frequency;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                frequency = local.calculateFrequency();
                stopLocalTimeCount();
                return frequency;
            case OFFLOADED:
                startSuspenseTimeCount();
                frequency = server.calculateFrequency();
                stopSuspenseTimeCount();
                return frequency;
            default:
                startLocalTimeCount();
                frequency = local.calculateFrequency();
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.calculateFrequency();
                stopSuspenseTimeCount();
                return frequency;
        }
    }

    @Override
    public void clearOldSignals(Long toTime) {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.clearOldSignals(toTime);
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.clearOldSignals(toTime);
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.clearOldSignals(toTime);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.clearOldSignals(toTime);
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public boolean areSlotsEqual() {
        boolean equal;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                equal = local.areSlotsEqual();
                stopLocalTimeCount();
                return equal;
            case OFFLOADED:
                startSuspenseTimeCount();
                equal = server.areSlotsEqual();
                stopSuspenseTimeCount();
                return equal;
            default:
                startLocalTimeCount();
                equal = local.areSlotsEqual();
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.areSlotsEqual();
                stopSuspenseTimeCount();
                return equal;
        }
    }

    @Override
    public void reduceTimeList(ArrayList<Long> seenTimeList, Long AveragePeriodHalfLength) {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.reduceTimeList(seenTimeList, AveragePeriodHalfLength);
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.reduceTimeList(seenTimeList, AveragePeriodHalfLength);
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.reduceTimeList(seenTimeList, AveragePeriodHalfLength);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.reduceTimeList(seenTimeList, AveragePeriodHalfLength);
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public void clearTimeList() {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.clearTimeList();
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.clearTimeList();
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.clearTimeList();
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.clearTimeList();
                stopSuspenseTimeCount();
                break;
        }
    }
}
