import classes.Utils.UtilsIntf;
import classes.Utils.UtilsProxy;
import classes.signal.SignalIntf;
import classes.signal.SignalProxy;
import classes.signalhandler.SignalHandlerIntf;
import classes.signalhandler.SignalHandlerProxy;
import org.opencv.core.Core;
import rmi.RmiServer;

public class Server {
    // Compulsory
    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    public static void main(String[] args) {

        // create the RMI rmi.server
        setupServer();
        SignalProxy.setExecutionLocal();
        SignalHandlerProxy.setExecutionLocal();
        UtilsProxy.setExecutionLocal();

    }

    private static void setupServer(){
        RmiServer rmiServer = new RmiServer();
        rmiServer.registerClassInterfaceToImplementation(UtilsIntf.class, UtilsProxy.class);
        rmiServer.registerClassInterfaceToImplementation(SignalHandlerIntf.class, SignalHandlerProxy.class);
        rmiServer.registerClassInterfaceToImplementation(SignalIntf.class, SignalProxy.class);
        rmiServer.start(6789);
        System.err.println("Server ready");
    }

}
