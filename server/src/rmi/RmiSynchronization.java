package rmi;

import java.lang.reflect.Proxy;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class RmiSynchronization {

    public Map<String, String> intfClassToImplClass = Collections.synchronizedMap(new HashMap<String, String>());
    private Map<String, Object> idToObject = Collections.synchronizedMap(new HashMap<String, Object>());

    public boolean regiterObject(Object o, String id){
        if(idToObject.containsValue(id)){
            return false;
        }
        else{
            idToObject.put(id, o);
            return true;
        }
    }

    @SuppressWarnings("unchecked")
    public <T> T createRemoteObject(Class<T> interfaceClass, String id, Class<?>[] argTypes, Object[] args){
        RmiClient remoteClient = new RmiClient(id, interfaceClass);
        if(argTypes != null){
            if(!remoteClient.initializeObject(id, argTypes, args)){
                throw new RmiException("Can't create remote object");
            }
        }
        else {
            if(!remoteClient.initializeObject(id, new Class<?>[]{}, new Object[]{})){
                throw new RmiException("Can't create remote object");
            }
        }
        return (T) Proxy.newProxyInstance(interfaceClass.getClassLoader(),
                new Class<?>[]{interfaceClass}, remoteClient);
    }

    @SuppressWarnings("unchecked")
    public <T> T getRemoteSingletonObject(Class<T> interfaceClass, String id){
        RmiClient remoteClient = new RmiClient(id, interfaceClass);
        if(!remoteClient.getSingletonObject()){
            throw new RmiException("Can't get singleton remote object");
        }
        return (T) Proxy.newProxyInstance(interfaceClass.getClassLoader(),
                new Class<?>[]{interfaceClass}, remoteClient);
    }

    public <T> T getRemoteObject(Class<T> interfaceClass, String id){
//        RmiClient remoteClient = new RmiClient(id, interfaceClass);
//        if(argTypes != null){
//            if(!remoteClient.initializeObject(id, argTypes, args)){
//                throw new RmiException("Can't create remote object");
//            }
//        }
//        else {
//            if(!remoteClient.initializeObject(id, new Class<?>[]{}, new Object[]{})){
//                throw new RmiException("Can't create remote object");
//            }
//        }
//        return (T) Proxy.newProxyInstance(interfaceClass.getClassLoader(),
//                new Class<?>[]{interfaceClass}, remoteClient);
    }

    public Object getObjectById(String id){
        if(idToObject.containsValue(id)){
            return idToObject.get(id);
        }
        else{
            return null;
        }
    }
}
