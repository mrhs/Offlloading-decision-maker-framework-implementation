package rmi;

import java.io.Serializable;

/**
 * An object wrapper for an actual method call.
 *
 * @author h.indzhov
 */
class RmiRequest implements Serializable {

    private static final long serialVersionUID = -5778642767500477993L;
    public static final int REGISTER_CLIENT = 20;
    public static final int CREATE_OBJECT = 0;
    public static final int GET_SINGLETON = 3;
    public static final int METHOD_INVOCATION = 1;
    public static final int DESTROY_OBJECT = 2;

    private Integer type;
    private String instanceId = "";
    private String interfaceName = "";
    private String methodName = "";
    private Class<?>[] argTypes = null;
    private Object[] args = null;

    public RmiRequest(){
        this.type = REGISTER_CLIENT;
    }

    public RmiRequest(String instanceId, String interfaceName, String methodName, Class<?>[] argTypes, Object[] args) {
        this.type = METHOD_INVOCATION;
        this.instanceId = instanceId;
        this.interfaceName = interfaceName;
        this.methodName = methodName;
        this.argTypes = argTypes;
        this.args = args;
    }

    public RmiRequest(String interfaceName, Class<?>[] argTypes, Object[] args) {
        this.interfaceName = interfaceName;
        this.type = CREATE_OBJECT;
        this.argTypes = argTypes;
        this.args = args;
    }

    public RmiRequest(String interfaceName) {
        this.interfaceName = interfaceName;
        this.type = GET_SINGLETON;
    }

    public RmiRequest(String interfaceName, String instanceId) {
        this.type = DESTROY_OBJECT;
        this.instanceId = instanceId;
    }

    public Integer getType() {
        return type;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public String getMethodName() {
        return methodName;
    }

    public Class<?>[] getArgTypes() {
        return argTypes;
    }

    public Object[] getArgs() {
        return args;
    }

}

