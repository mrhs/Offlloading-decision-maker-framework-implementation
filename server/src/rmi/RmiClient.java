package rmi;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.Socket;

/**
 * RcpClient
 *
 * @author h.indzhov
 */
public class RmiClient implements InvocationHandler {

    protected static final int PORT = 6789;
    protected static String hostIp = null;
    private static Boolean isRegisteredOnServer = false;

    public static void manualServerRegistration(String serverIp) {
        RmiClient.isRegisteredOnServer = true;
        hostIp = serverIp;
    }

    private String instanceId;
    private Class<?> interfaceClass;

    public String getInstanceId() {
        return instanceId;
    }

    public Class<?> getInterfaceClass() {
        return interfaceClass;
    }

    public RmiClient(String id, Class<?> interfaceClass) {
        if (!isRegisteredOnServer) {
            if (registerOnServer())
                isRegisteredOnServer = true;
            else
                throw new RmiException("Can't register on server");
        }
        this.instanceId = id;
        this.interfaceClass = interfaceClass;
    }

    public static Boolean setHostIp(String ip) {
        if (hostIp == null) {
            hostIp = ip;
            return true;
        } else {
            return false;
        }
    }



    private static Boolean registerOnServer() {
        try {
            RmiRequest rmiRequest = new RmiRequest();

            Socket clientSocket = new Socket(hostIp, PORT);
            writeRequestObject(rmiRequest, clientSocket);
            RmiResponse rmiResponse = readResponseObject(clientSocket);

            if (!rmiResponse.isSuccessfull()) {
                throw new RmiException(rmiResponse.getException());
            }
            return (Boolean) rmiResponse.getReturnValue();

        } catch (Exception e) {
            throw new RmiException(e);
        }
    }

    private static RmiResponse readResponseObject(Socket clientSocket)
            throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(
                clientSocket.getInputStream());
        RmiResponse rmiResponse = (RmiResponse) ois.readObject();
        clientSocket.shutdownInput();
        clientSocket.close();
        return rmiResponse;
    }

    private static void writeRequestObject(RmiRequest rmiRequest, Socket clientSocket)
            throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(
                clientSocket.getOutputStream());
        oos.writeObject(rmiRequest);
        clientSocket.shutdownOutput();
    }

    public Boolean initializeObject(String id, Class<?>[] argTypes, Object[] args) {
        try {
            RmiRequest rmiRequest = new RmiRequest(interfaceClass.getName(), argTypes, args);

            Socket clientSocket = new Socket(hostIp, PORT);
            writeRequestObject(rmiRequest, clientSocket);
            RmiResponse rmiResponse = readResponseObject(clientSocket);

            if (!rmiResponse.isSuccessfull()) {
                throw new RmiException(rmiResponse.getException());
            }
            return (Boolean) rmiResponse.getReturnValue();

        } catch (Exception e) {
            throw new RmiException(e);
        }

    }

    public Boolean getSingletonObject() {
        try {
            RmiRequest rmiRequest = new RmiRequest(interfaceClass.getName());

            Socket clientSocket = new Socket(hostIp, PORT);
            writeRequestObject(rmiRequest, clientSocket);
            RmiResponse rmiResponse = readResponseObject(clientSocket);

            if (!rmiResponse.isSuccessfull()) {
                throw new RmiException(rmiResponse.getException());
            }
            return (Boolean) rmiResponse.getReturnValue();

        } catch (Exception e) {
            throw new RmiException(e);
        }

    }

    public Object invoke(Object proxy, Method method, Object[] args) {
        try {
            String interfaceName = interfaceClass.getName();

            String methodName = method.getName();
            Class<?>[] argTypes = method.getParameterTypes();

            RmiRequest rmiRequest = new RmiRequest(instanceId, interfaceName, methodName, argTypes, args);

            Socket clientSocket = new Socket(hostIp, PORT);
            writeRequestObject(rmiRequest, clientSocket);
            RmiResponse rmiResponse = readResponseObject(clientSocket);

            if (!rmiResponse.isSuccessfull()) {
                throw new RmiException(rmiResponse.getException());
            }

            Object returnValue = rmiResponse.getReturnValue();
            return returnValue;
        } catch (Exception e) {
            throw new RmiException(e);
        }
    }

}
