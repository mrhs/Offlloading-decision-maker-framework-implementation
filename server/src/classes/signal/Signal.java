package classes.signal;

import org.opencv.core.Point;

import java.util.ArrayList;

/**
 * Created by mrhs on 11/9/2017.
 */

public class Signal implements SignalIntf {

    //********************************** DATA **********************************
    private Point currentLocation;
    private double currentArea;
    private Long firstSeenTime;
    private Long lastSeenTime;

    private float frequency = -1;
    private float dutyCycle = -1;

    private ArrayList<Long> seenTimeList;
    private ArrayList<TimeSlot> signalList;

    //********************************** FUNCTION **********************************

    //********************** Constructor **********************

    public Signal(Point currentLocation, Double currentArea, Long firstSeenTime) {
        this.currentLocation = currentLocation;
        this.currentArea = currentArea;
        this.firstSeenTime = firstSeenTime;
        this.lastSeenTime = firstSeenTime;
        this.seenTimeList = new ArrayList<>();
        this.signalList = new ArrayList<>();
    }

    //********************** Setter **********************

    @Override
    public void setCurrentCondition(Point center, Double area) {
        setCurrentLocation(center);
        setCurrentArea(area);
    }

    @Override
    public void addToSeenTimeList(Long timeSeen) {
        this.seenTimeList.add(timeSeen);
        lastSeenTime = timeSeen;
    }

    @Override
    public Point getCurrentLocation() {
        return currentLocation;
    }

    @Override
    public void setCurrentLocation(Point currentLocation) {
        this.currentLocation = currentLocation;
    }

    @Override
    public double getCurrentArea() {
        return currentArea;
    }

    @Override
    public void setCurrentArea(double currentArea) {
        this.currentArea = currentArea;
    }

    @Override
    public Long getFirstSeenTime() {
        return firstSeenTime;
    }

    @Override
    public void setFirstSeenTime(Long firstSeenTime) {
        this.firstSeenTime = firstSeenTime;
    }

    @Override
    public Long getLastSeenTime() {
        return lastSeenTime;
    }

    @Override
    public void setLastSeenTime(Long lastSeenTime) {
        this.lastSeenTime = lastSeenTime;
    }

    //********************** Getter **********************

    @Override
    public ArrayList<Long> getSeenTimeList() {
        return seenTimeList;
    }

    @Override
    public void setSeenTimeList(ArrayList<Long> seenTimeList) {
        this.seenTimeList = seenTimeList;
    }

    @Override
    public float getFrequency() {
        return frequency;
    }

    @Override
    public void setFrequency(float frequency) {
        this.frequency = frequency;
    }

    @Override
    public float getDutyCycle() {
        return dutyCycle;
    }

    @Override
    public void setDutyCycle(float dutyCycle) {
        this.dutyCycle = dutyCycle;
    }

    @Override
    public ArrayList<TimeSlot> getSignalList() {
        return signalList;
    }

    @Override
    public void setSignalList(ArrayList<TimeSlot> signalList) {
        this.signalList = signalList;
    }

    //******************************* Utility functions *******************************
    @Override
    public float calculateDutyCycle() {
        if (signalList.size() > 2) {
            double sum = 0, total = signalList.get(signalList.size() - 1).first - signalList.get(0).first;
            for (int i = 0; i < signalList.size() - 1; i++) {
                sum += signalList.get(i).diff;
            }
            return (float) (sum / total);
        } else {
            return -1;
        }
    }

    @Override
    public float calculateFrequency() {
        if (signalList.size() > 2) {
            double total = signalList.get(signalList.size() - 1).first - signalList.get(0).first;
            return (float) ((signalList.size() - 1) / total) * 1000;
        } else {
            return -1;
        }
    }


    @Override
    public void clearOldSignals(Long toTime) {
        ArrayList<TimeSlot> tmpsignalList = new ArrayList<>();
        for (TimeSlot timeSlot : this.signalList) {
            if (timeSlot.last > toTime) {
                tmpsignalList.add(timeSlot);
            }
        }
        this.signalList = tmpsignalList;
    }

    // This function is for checking if the time slots are equal or not

    @Override
    public boolean areSlotsEqual() {
        return false;
    }


    @Override
    public void reduceTimeList(ArrayList<Long> seenTimeList, Long AveragePeriodHalfLength) {
        if (this.seenTimeList.size() > 2) {
            long startTime = this.seenTimeList.get(0) - AveragePeriodHalfLength;
            long endTime;
            int localIndex = 0;
            int mainIndex = seenTimeList.indexOf(this.seenTimeList.get(localIndex));
            while (mainIndex < seenTimeList.size() && localIndex < this.seenTimeList.size()) {
                //When signal is active
                if (mainIndex < seenTimeList.size() && this.seenTimeList.get(localIndex) == seenTimeList.get(mainIndex)) {
                    mainIndex++;
                    localIndex++;
                    continue;
                }
                //When signal is inactive
                else {
                    endTime = this.seenTimeList.get(localIndex - 1) + AveragePeriodHalfLength;
                    signalList.add(new TimeSlot(startTime, endTime));
                    while (mainIndex < seenTimeList.size() && this.seenTimeList.get(localIndex) != seenTimeList.get(mainIndex)) {
                        mainIndex++;
                    }
                    startTime = this.seenTimeList.get(localIndex) - AveragePeriodHalfLength;
                }
            }
            //Calculate signal characteristics
            frequency = calculateFrequency();
            dutyCycle = calculateDutyCycle();

            if (signalList.size() > 1) {
                clearTimeList();
            }
        }
    }

    @Override
    public void clearTimeList() {
        long last = signalList.get(signalList.size() - 1).last;
        ArrayList<Long> tmpSeenTimeList = new ArrayList<>();
        for (Long seenTime : this.seenTimeList) {
            if (seenTime > last) {
                tmpSeenTimeList.add(seenTime);
            }
        }
        this.seenTimeList = tmpSeenTimeList;
    }

    //********************************** TIME FRAME **********************************
    public class TimeSlot {
        Long first, last, diff;

        public TimeSlot(Long first, Long last) {
            this.first = first;
            this.last = last;
            this.diff = last - first;
        }
    }

}
