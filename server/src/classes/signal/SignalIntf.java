package classes.signal;

import org.opencv.core.Point;

import java.util.ArrayList;

/**
 * Created by Muhammad Reza on 21/08/2018.
 */

public interface SignalIntf {
    void setCurrentCondition(Point center, Double area);

    void setCurrentLocation(Point currentLocation);

    void setCurrentArea(double currentArea);

    void setFirstSeenTime(Long firstSeenTime);

    void setLastSeenTime(Long lastSeenTime);

    void addToSeenTimeList(Long timeSeen);

    void setFrequency(float frequency);

    void setDutyCycle(float dutyCycle);

    void setSeenTimeList(ArrayList<Long> seenTimeList);

    void setSignalList(ArrayList<Signal.TimeSlot> signalList);

    Point getCurrentLocation();

    double getCurrentArea();

    Long getFirstSeenTime();

    Long getLastSeenTime();

    ArrayList<Long> getSeenTimeList();

    float getFrequency();

    float getDutyCycle();

    ArrayList<Signal.TimeSlot> getSignalList();

    //******************************* Utility functions *******************************
    float calculateDutyCycle();

    float calculateFrequency();

    void clearOldSignals(Long toTime);

    boolean areSlotsEqual();

    void reduceTimeList(ArrayList<Long> seenTimeList, Long AveragePeriodHalfLength);

    void clearTimeList();
}
