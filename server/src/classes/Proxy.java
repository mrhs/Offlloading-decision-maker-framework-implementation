package classes;



/**
 * Created by Muhammad Reza on 21/08/2018.
 */

public abstract class Proxy<T> {
    //********************************** STATIC **********************************
    protected static final int LOCAL = 0;
    protected static final int OFFLOADED = 1;
    protected static final int BOTH = 2;

    //********************************** DATA **********************************
    protected Integer localStartTime, suspenseStartTime;
    protected T local, server;
    protected String id;

    //********************************** FUNCTION **********************************


    public Proxy(String id) {
        System.out.println("Proxy: " + id);
        this.id = id;
    }

    protected void finalize() {
    }

    public void stopLocalTimeCount() {
        incrementTotalLocalTime((int) System.currentTimeMillis() - localStartTime);
    }

    public void startLocalTimeCount() {
        localStartTime = (int) System.currentTimeMillis();
    }

    public void startSuspenseTimeCount() {
        suspenseStartTime = (int) System.currentTimeMillis();
    }

    public void stopSuspenseTimeCount() {
        incrementTotalSuspendedTime((int) System.currentTimeMillis() - suspenseStartTime);
    }

    //********************** Setter **********************

    public abstract void incrementTotalLocalTime(Integer totalLocalTimeV);

    public abstract void incrementTotalSuspendedTime(Integer totalSuspendedTimeV);

    //********************** Getter **********************
    public String getId() {
        return id;
    }
}

//switch (executionCondition) {
//        case LOCAL:
//        startLocalTimeCount();
//
//        stopLocalTimeCount();
//        break;
//        case OFFLOADED:
//        startSuspenseTimeCount();
//
//        stopSuspenseTimeCount();
//        break;
//default:
//        startLocalTimeCount();
//
//        stopLocalTimeCount();
//        startSuspenseTimeCount();
//
//        stopSuspenseTimeCount();
//        break;
//        }