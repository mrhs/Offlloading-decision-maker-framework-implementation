package classes.Utils;

import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;

import java.util.List;

import classes.Proxy;
import rmi.RmiClient;

/**
 * Created by Muhammad Reza on 21/08/2018.
 */

public class UtilsProxy extends Proxy<UtilsIntf> implements UtilsIntf {

    //****************************** Profiling *************************************
    protected static Integer totalLocalTime = 0;
    protected static Integer totalSuspendedTime = 0;

    private static UtilsProxy ourInstance = null;

    private static final int MAT_WIDTH = 340;
    private static final int MAT_HEIGHT = 150;

    protected static Integer executionCondition = BOTH; //At the beginning the execution will be on both server and local

    //********************** Setter **********************

    public synchronized static void setExecutionLocal() {
        executionCondition = LOCAL;
    }

    public synchronized static void setExecutionOffloaded() {
        executionCondition = OFFLOADED;
    }

    public synchronized static void setExecutionBoth() {
        executionCondition = BOTH;
    }

    @Override
    public synchronized void incrementTotalLocalTime(Integer totalLocalTimeV) {
        totalLocalTime += totalLocalTimeV;
    }

    @Override
    public synchronized void incrementTotalSuspendedTime(Integer totalSuspendedTimeV) {
        totalSuspendedTime += totalSuspendedTimeV;
    }

    public synchronized static void resetTotalLocalTime() {
        totalLocalTime = 0;
    }

    public synchronized static void resetTotalSuspendedTime() {
        totalSuspendedTime = 0;
    }


    //********************** Getter **********************

    public static Integer getTotalLocalTime() {
        return totalLocalTime;
    }

    public static Integer getTotalSuspenseTime() {
        return totalSuspendedTime;
    }

    public static Integer getTotalOffloadedTime() {
        return 0;
    }

    ;

    //******************************************************************************

    public static UtilsProxy getInstance() {
        if(ourInstance == null){
            ourInstance = new UtilsProxy();
        }
        return ourInstance;
    }

    private UtilsProxy() {
        super(UtilsIntf.class.getSimpleName());
        local = new Utils();
        server = RmiClient.constructor(UtilsIntf.class, true);
    }

    @Override
    public byte[] processMat(byte[] inputMat) {
        byte[] mat;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                mat = local.processMat(inputMat);
                stopLocalTimeCount();
                return mat;
            case OFFLOADED:
                startSuspenseTimeCount();
                mat = server.processMat(inputMat);
                stopSuspenseTimeCount();
                return mat;
            default:
                startLocalTimeCount();
                mat = local.processMat(inputMat);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.processMat(inputMat);
                stopSuspenseTimeCount();
                return mat;
        }
    }

    @Override
    public void initialize() {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.initialize();
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.initialize();
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.initialize();
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.initialize();
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public void destroy() {
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                local.destroy();
                stopLocalTimeCount();
                break;
            case OFFLOADED:
                startSuspenseTimeCount();
                server.destroy();
                stopSuspenseTimeCount();
                break;
            default:
                startLocalTimeCount();
                local.destroy();
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.destroy();
                stopSuspenseTimeCount();
                break;
        }
    }

    @Override
    public Point centerOfContour(MatOfPoint contur) {
        Point point;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                point = local.centerOfContour(contur);
                stopLocalTimeCount();
                return point;
            case OFFLOADED:
                startSuspenseTimeCount();
                point = server.centerOfContour(contur);
                stopSuspenseTimeCount();
                return point;
            default:
                startLocalTimeCount();
                point = local.centerOfContour(contur);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.centerOfContour(contur);
                stopSuspenseTimeCount();
                return point;
        }
    }

    @Override
    public List<MatOfPoint> filterSmallContours(List<MatOfPoint> contours) {
        List<MatOfPoint> mat;
        switch (executionCondition) {
            case LOCAL:
                startLocalTimeCount();
                mat = local.filterSmallContours(contours);
                stopLocalTimeCount();
                return mat;
            case OFFLOADED:
                startSuspenseTimeCount();
                mat = server.filterSmallContours(contours);
                stopSuspenseTimeCount();
                return mat;
            default:
                startLocalTimeCount();
                mat = local.filterSmallContours(contours);
                stopLocalTimeCount();
                startSuspenseTimeCount();
                server.filterSmallContours(contours);
                stopSuspenseTimeCount();
                return mat;
        }
    }
}
