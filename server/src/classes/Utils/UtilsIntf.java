package classes.Utils;

import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;

import java.util.List;

/**
 * Created by Muhammad Reza on 21/08/2018.
 */

public interface UtilsIntf {
    byte[] processMat(byte[] inputMat);

    void destroy();

    void initialize();

    //***************************** Utility functions *****************************
    Point centerOfContour(MatOfPoint contur);

    List<MatOfPoint> filterSmallContours(List<MatOfPoint> contours);
}
