package classes.Utils;


import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.imgproc.Moments;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import classes.signal.SignalProxy;
import classes.signalhandler.SignalHandlerProxy;

/**
 * Created by Muhammad Reza on 20/08/2018.
 */

public class Utils implements UtilsIntf {
    //********************************** STATIC **********************************
    private static final int CONTOUR_AREA_MIN = 50; //TODO this should be relative to cam picture size
    private static final int MAT_WIDTH = 340;
    private static final int MAT_HEIGHT = 150;
    //********************************** DATA **********************************
    private Mat mainMat, hsvMat, mask, morphOutput, temp_mat;
    private SignalHandlerProxy signalHandler;

    //********************************** FUNCTION **********************************

    @Override
    public byte[] processMat(byte[] inputMat) {
        // remove some noise
        Imgproc.blur(convertByteArray2Mat(inputMat), mainMat, new Size(2, 2));

        // convert the frame to HSV
        Imgproc.cvtColor(mainMat, hsvMat, Imgproc.COLOR_BGR2HSV);

        // get thresholding values from the UI
        // remember: H ranges 0-180, S and V range 0-255
        Scalar minValues = new Scalar(44, 100, 100);
        Scalar maxValues = new Scalar(80, 255, 255);

        // threshold HSV image to select tennis balls
        Core.inRange(hsvMat, minValues, maxValues, mask);

        // morphological operators
        // dilate with large element, erode with small ones
        Integer amount = 6;
        Mat dilateElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(amount * 2, amount * 2));
        Mat erodeElement = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(amount, amount));

        Imgproc.erode(mask, morphOutput, erodeElement);
        Imgproc.erode(mask, morphOutput, erodeElement);

        Imgproc.dilate(mask, morphOutput, dilateElement);
        Imgproc.dilate(mask, morphOutput, dilateElement);

        // init object tracking
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();

        // find contours
        Imgproc.findContours(morphOutput, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

        //Find the biggest contour
//        Collections.max(contours, new CustomComparator());

        //Calculate the center of the contours and show them
        ArrayList<Point> centerOfContours = new ArrayList();
        ArrayList<Double> areaOfContours = new ArrayList();
        for (MatOfPoint i : contours) {
            double area = Imgproc.contourArea(i);
            if (area > CONTOUR_AREA_MIN) { //Filter very small contours
                centerOfContours.add(centerOfContour(i));
                areaOfContours.add(area);
                Imgproc.circle(mainMat, centerOfContour(i), 2, new Scalar(255, 255, 255), -1);
            }
        }

        // Initializing step for signal handler
        signalHandler.setStep(centerOfContours, areaOfContours);
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(1);
        for (SignalProxy signal : signalHandler.getSignalsList()) {
            Imgproc.putText(mainMat, "F: " + df.format(signal.getFrequency()) + " DC: " + df.format(signal.getDutyCycle()), signal.getCurrentLocation(), Core.FONT_ITALIC, 0.5,
                    new Scalar(255, 255, 255), 2);
//            Imgproc.putText(mainMat, "" + df.format(signal.getCurrentArea()), signal.getCurrentLocation(), Core.FONT_ITALIC, 0.5,
//                    new Scalar(255, 255, 255), 2);
        }

        // Draw the shape
        for (SignalProxy signal : signalHandler.getDesignatedSignal((float) 1, (float) 0.1, (float) 0.5, (float) 0.1)) {
            Imgproc.circle(mainMat, signal.getCurrentLocation(), (int) (Math.sqrt(signal.getCurrentArea()) / 2), new Scalar(255, 0, 0), 3);
        }


        return convertMat2ByteArray(mainMat); // This function must return
    }

    @Override
    public void initialize() {
        mainMat = new Mat(MAT_HEIGHT, MAT_WIDTH, CvType.CV_8UC4);
        hsvMat = new Mat(MAT_HEIGHT, MAT_WIDTH, CvType.CV_8UC4);
        mask = new Mat(MAT_HEIGHT, MAT_WIDTH, CvType.CV_8UC4);
        morphOutput = new Mat(MAT_HEIGHT, MAT_WIDTH, CvType.CV_8UC4);
        temp_mat = new Mat(288, 352, 24);

        signalHandler = new SignalHandlerProxy();

        signalHandler = new SignalHandlerProxy();
    }

    @Override
    public void destroy() {
        mainMat.release();
        hsvMat.release();
        mask.release();
        morphOutput.release();
    }

    //***************************** Utility functions *****************************
    @Override
    public Point centerOfContour(MatOfPoint contur) {
        Moments M = Imgproc.moments(contur);
        return new Point(M.get_m10() / M.get_m00(), M.get_m01() / M.get_m00());
    }

    @Override
    public List<MatOfPoint> filterSmallContours(List<MatOfPoint> contours) {
        List<MatOfPoint> contoursOutput = new ArrayList<>();
        for (MatOfPoint i : contours) {
            double area = Imgproc.contourArea(i);
            if (area > CONTOUR_AREA_MIN) { //Filter very small contours
                contoursOutput.add(i);
            }
        }
        return contoursOutput;
    }

    private Mat convertByteArray2Mat(byte[] array) {
        temp_mat.put(0, 0, array);
        return temp_mat;
    }

    private byte[] convertMat2ByteArray(Mat mat) {
        byte[] return_buff = new byte[(int) (mat.total() * mat.elemSize())];
        mat.get(0, 0, return_buff);
        return return_buff;
    }
}
